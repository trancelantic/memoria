﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    private int _settings;
    private const int SettingsNumber = 3;

    public enum EPairNumber
    {
        NotSet = 0,
        E10Pairs = 10,
        E15Pairs = 15,
        E20Pairs = 20,
        
    }

    public enum EMemoriaCategories
    {
        NotSet,
        Fruits,
        Transport,
    }

    public struct Settings
    {
        public EPairNumber PairsNumber;
        public EMemoriaCategories MemoriaCategory;
    };

    private Settings _gameSettings;

    public static GameSettings Instance; 

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this);
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

    }

    void Start()
    {
        _gameSettings = new Settings();
        ResetGameSettings();
    }

    public void SetPairNumber(EPairNumber Number)

    {
        if (_gameSettings.PairsNumber == EPairNumber.NotSet)
        _settings++;

        _gameSettings.PairsNumber = Number;
    }

    public void SetMemoriaCategories(EMemoriaCategories cat)
    {
        if(_gameSettings.MemoriaCategory == EMemoriaCategories.NotSet)
        _settings++;

        _gameSettings.MemoriaCategory = cat;
    }
    public EPairNumber GetPairNumber()
    {
        return _gameSettings.PairsNumber;
    }

    public void ResetGameSettings()
    {
        _settings = 0;
        _gameSettings.MemoriaCategory = EMemoriaCategories.NotSet;
        _gameSettings.PairsNumber = EPairNumber.NotSet;

    }
     public bool AllSettingsReady()
    {
        return _settings == SettingsNumber;
    }
}
