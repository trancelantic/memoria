﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGameButton : MonoBehaviour
{
    public enum EButtonType
    {
        NotSet,
        PairNumberBtn,
        MemoriaCategoryBtn,
    };

    [SerializedField] public EButtonType ButtonType = EButtonType.NotSet;
    [HideInspector] public GameSettings.EPairNumber PairNumber = GameSetting.EPairNumber.NotSet;
    [HideInspector] public GameSettings.MemoriaCategories MemoriaCategories = GameSetting.MemoriaCategories.NotSet;

    // Start is called before the first frame update
    void Start()
    {
        
    }

   
        
    public void SetGameOption(string GameSceneName)
    {
        var comp = gameObject. GetComponent<SetGameButton>();
        switch (comp.ButtonType)
        {
            case SetGameButton.EButtonType.PairNumberBtn:
                GameSettings.Instance.SetPairNumber(comp.PairNumber);
                break;

            case EButtonType.MemoriaCategoryBtn:
                GameSettings.Instance.SetMemoriaCategories(comp.MemoriaCategories);
                break;

        }
        if(GameSettings.Instance.AllSettingsReady())
        {
            SceneManager.LoadScene(GameSceneName);
        }
       
    }
}
